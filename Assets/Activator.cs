﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Activator
/// </summary>
public class Activator : MonoBehaviour {
	#region Fields

	public GameObject[] objects;

	private void OnTriggerEnter( Collider other ) {
		foreach (GameObject o in objects) {
			o.GetComponent<MyAgent>().enabled = true;
		}
	}

	#endregion


}