﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: KidAgent
/// </summary>
public class KidAgent : MonoBehaviour {
	#region Fields

	public GameObject[] kids;
	static float tagTimer = 0;
	public bool isLeader = false;

	#endregion

	void Update() {
		if (isLeader) {
			tagTimer -= Time.deltaTime;
		}
	}

	void OnCollisionEnter(Collision other) {
		if (isLeader) {
			if (other.transform.tag == "Kid") {
				if (tagTimer <= 0) {
					isLeader = false;
					other.transform.GetComponent<MyAgent>().agentState = AgentState.Wander;
					other.transform.GetComponent<MyAgent>().target = null;
					other.transform.GetComponent<MyAgent>().maxSpeed = 10;

					other.transform.GetComponent<KidAgent>().isLeader = true;
					
					foreach (GameObject kid in kids) {
						if (!kid.GetComponent<KidAgent>().isLeader) {
							//print(kid.name + " is not other, which is " + other.transform.name);
							kid.GetComponent<MyAgent>().agentState = AgentState.Seek;
							kid.GetComponent<MyAgent>().target = other.transform;
							kid.GetComponent<MyAgent>().maxSpeed = 7;
						}
						tagTimer = 2;
					}
				}
			}
		}
	}

}