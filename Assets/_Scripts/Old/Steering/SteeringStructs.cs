﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SteeringStructs
/// </summary>
public class Steering {
	#region Fields

	public float angular;
	public Vector3 linear;

	#endregion

	public Steering() {
		angular = 0.0f;
		linear = new Vector3();
	}
}