﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Agent
/// </summary>
[RequireComponent(typeof(OldAgent))]
public class AgentBehaviour : MonoBehaviour {
	#region Fields

	public GameObject target;
	protected OldAgent oldAgent;

	public float maxSpeed;
	public float maxAccel;
	public float maxRotation;
	public float maxAngularAccel;

	#endregion

	public virtual void Awake() {
		oldAgent = GetComponent<OldAgent>();
	}

	public virtual void Update() {
		oldAgent.SetSteering(GetSteering());
	}

	public virtual Steering GetSteering() {
		return new Steering();
	}

	public float MapToRange( float rotationToMap ) {
		rotationToMap %= 360.0f;
		if ( Mathf.Abs( rotationToMap ) > 180.0f ) {
			if ( rotationToMap < 0.0f ) {
				rotationToMap += 360.0f;
			} else {
				rotationToMap -= 360.0f;
			}
		}
		return rotationToMap;
	}

	public Vector3 OrientationAsVector(float orientation) {
		Vector3 vector = Vector3.zero;
		vector.x = Mathf.Sin(orientation * Mathf.Rad2Deg) * 1.0f;
		vector.z = Mathf.Cos(orientation * Mathf.Rad2Deg) * 1.0f;
		return vector.normalized;
	}

}