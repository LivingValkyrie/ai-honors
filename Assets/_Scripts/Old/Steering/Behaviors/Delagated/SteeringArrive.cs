﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SteeringArrive
/// </summary>
public class SteeringArrive : AgentBehaviour {
	#region Fields

	public float targtRadius, slowRadius, timeToTarget;

	#endregion

	public override Steering GetSteering() {
		Steering steering = new Steering();
		Vector3 direction = target.transform.position - transform.position;
		float distance = direction.magnitude;
		float targetspeed;
		if (distance < targtRadius) {
			return steering;
		}

		if (distance > slowRadius) {
			targetspeed = oldAgent.maxSpeed;
		} else {
			targetspeed = oldAgent.maxSpeed * (distance / slowRadius);
		}

		Vector3 targetVelocity = direction.normalized * targetspeed;
		steering.linear = targetVelocity - oldAgent.velocity;
		steering.linear /= timeToTarget;
		if (steering.linear.magnitude > oldAgent.maxAccel) {
			steering.linear = steering.linear.normalized * oldAgent.maxAccel;
		}
		
		return steering;
	}

	void OnDrawGizmos() {
		if (target) {
			
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(target.transform.position, slowRadius);

		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(target.transform.position, targtRadius);
		}
	}
}