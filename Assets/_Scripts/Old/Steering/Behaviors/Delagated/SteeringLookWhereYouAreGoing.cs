﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SteeringLookWhereYouAreGoing
/// </summary>
public class SteeringLookWhereYouAreGoing : SteeringAlign {
	#region Fields

	GameObject auxTarget;

	#endregion

	public override void Awake() {
		base.Awake();
		auxTarget = target;
		target = new GameObject( "Look target" );
		target.AddComponent<OldAgent>();
	}

	public override Steering GetSteering() {
		if (oldAgent.velocity.magnitude != 0) {
			target.GetComponent<OldAgent>().orientation = Mathf.Atan2(oldAgent.velocity.x, oldAgent.velocity.z);
		}

		return base.GetSteering();
	}
}