﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SteeringWander
/// </summary>
public class SteeringWander : SteeringFace {
	#region Fields

	public float wanderOffset;
	public float wanderRadius;
	public float wanderRate;
	public float wanderOrientation;

	#endregion

	void Start() {
		target = new GameObject("Wander Target");
		target.AddComponent<OldAgent>();
	}

	public override Steering GetSteering() {
		wanderOrientation += Kinematic.RandomBinomial() * wanderRate;
		float TargetOrientation = wanderOrientation + oldAgent.orientation;
		target.transform.position = transform.position + wanderOffset * OrientationAsVector(oldAgent.orientation);

		target.transform.position += wanderRadius * OrientationAsVector(TargetOrientation);

		Steering steering = base.GetSteering();

		steering.linear = maxAccel * OrientationAsVector(oldAgent.orientation);

		return steering;
	}
}