﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SteeringPursue
/// </summary>
public class SteeringPursue : SteeringSeek {
	#region Fields

	public float maxPrediction;
	GameObject auxTarget;
	OldAgent targetOldAgent;

	#endregion

	public override void Awake() {
		base.Awake();

		targetOldAgent = target.GetComponent<OldAgent>();
		auxTarget = target;
		target = new GameObject("PursueTarget");
	}

	public override Steering GetSteering() {
		Steering steering = new Steering();

		Vector3 direction = target.transform.position - transform.position;
		float distance = direction.magnitude;

		float speed = oldAgent.velocity.magnitude;

		float prediction;
		if (speed <= distance / maxPrediction) {
			prediction = maxPrediction;
		} else {
			prediction = distance / speed;
		}

		target.transform.position = auxTarget.transform.position;
		target.transform.position += targetOldAgent.velocity * prediction;

		return base.GetSteering();
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.red;
		if (target) {
			Gizmos.DrawSphere(target.transform.position, .5f);
		}
	}

	void OnDestroy() {
		Destroy(auxTarget);
	}

}