﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SteeringFace
/// </summary>
public class SteeringFace : SteeringAlign {
	#region Fields

	GameObject auxTarget;

	#endregion

	public override void Awake() {
		base.Awake();
		auxTarget = target;
		target = new GameObject("Face target");
		target.AddComponent<OldAgent>();
	}

	public override Steering GetSteering() {
		Vector3 direction = auxTarget.transform.position - transform.position;
		
		if (direction.magnitude > 0.0f) {
			float targetOrientation = Mathf.Atan2(direction.x, direction.z);
			targetOrientation *= Mathf.Rad2Deg;
			target.GetComponent<OldAgent>().orientation = targetOrientation;
		}

		return base.GetSteering();
	} 

	void OnDestroy() {
		Destroy(target);
	}
}