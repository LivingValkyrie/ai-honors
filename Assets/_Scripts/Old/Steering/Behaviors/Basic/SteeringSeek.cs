﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SteeringSeek
/// </summary>
public class SteeringSeek : AgentBehaviour {
	#region Fields

	#endregion

	public override Steering GetSteering() {
		Steering steering = new Steering();
		steering.linear = target.transform.position - transform.position;
		steering.linear.Normalize();
		steering.linear = steering.linear * oldAgent.maxAccel;
		return steering;
	}
}