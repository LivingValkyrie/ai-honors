﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SteeringAlign
/// </summary>
public class SteeringAlign : AgentBehaviour {
	#region Fields

	public float targetAngle;
	public float slowAngle;
	public float timeToTarget = 0.1f;

	#endregion

	public override Steering GetSteering() {
		Steering steering = new Steering();

		if (target.GetComponent<OldAgent>()) {
			float targetOrientation = target.GetComponent<OldAgent>().orientation;
			float rotation = targetOrientation - oldAgent.orientation;
			rotation = MapToRange(rotation);
			float rotationSize = Mathf.Abs(rotation);
			if (rotationSize < slowAngle) {
				return steering;
			}

			float targetRotation;
			if (rotationSize > slowAngle) {
				targetRotation = maxRotation;
			} else {
				targetRotation = maxRotation * (rotationSize / slowAngle);
			}
			targetRotation *= rotation / rotationSize;

			steering.angular = targetRotation - oldAgent.rotation;
			steering.angular /= timeToTarget;
			float angularAccel = Mathf.Abs(steering.angular);
			if (angularAccel > maxAngularAccel) {
				steering.angular /= angularAccel;
				steering.angular *= maxAngularAccel;
			}
		}

		return steering;
	}

}