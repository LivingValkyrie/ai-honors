﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SteeringVelocityMatch
/// </summary>
public class SteeringVelocityMatch : AgentBehaviour {
	#region Fields

	public float timeToTarget = 0.1f;

	#endregion
	
	public override Steering GetSteering() {
		Steering steering = new Steering();

		steering.linear = target.GetComponent<OldAgent>().velocity - oldAgent.velocity;
		steering.linear /= timeToTarget;

		if (steering.linear.magnitude > maxAccel) {
			steering.linear = steering.linear.normalized * maxAccel;
		}

		steering.angular = 0;

		return steering;
	}
}