﻿using UnityEngine;
using UnityEngine.UI;

public struct Static {
	public Vector2 position;
	public float orientation;
}

public struct Kinematic {
	public Vector3 position;
	public float orientation;
	public Vector3 velocity;
	public float rotation;

	public void Update(SteeringOutput steering, float deltaTime, bool useNewtonEulerOne = true) {
		if (useNewtonEulerOne) {
			position += velocity * deltaTime;
			orientation += rotation * deltaTime;
		} else {
			position += (velocity * deltaTime) + (0.5f * steering.linear * deltaTime * deltaTime);
			orientation += (rotation * deltaTime) + (0.5f * steering.angular * deltaTime * deltaTime);
		}

		velocity += steering.linear * deltaTime;
		orientation += steering.angular * deltaTime;
	}

	public void NewtonEulerUpdate(SteeringOutput steering, float deltaTime) {
		position += velocity * deltaTime;
		orientation += rotation * deltaTime;

		velocity += steering.linear * deltaTime;
		orientation += steering.angular * deltaTime;
	}

	public static float GetNewOrientation(float currOrientation, Vector3 currVelocity) {
		if (currVelocity.magnitude > 0) {
			Mathf.Atan2(-currVelocity.x, currVelocity.z);
			return 0;
		} else {
			return currOrientation;
		}
	}

	public KinematicSteeringOutput GetSteeringSeek(Kinematic target, float maxSpeed) {
		KinematicSteeringOutput steering = new KinematicSteeringOutput();

		steering.velocity = target.position - position;
		Debug.DrawRay(position, steering.velocity);

		steering.velocity = steering.velocity.normalized;
		steering.velocity *= maxSpeed;

		orientation = Kinematic.GetNewOrientation(orientation, steering.velocity);

		steering.rotation = 0;
		return steering;
	}

	public KinematicSteeringOutput GetSteeringFlee(Kinematic target, float maxSpeed) {
		KinematicSteeringOutput steering = new KinematicSteeringOutput();

		steering.velocity = position - target.position;
		Debug.DrawRay(position, steering.velocity);

		steering.velocity = steering.velocity.normalized;
		steering.velocity *= maxSpeed;

		orientation = Kinematic.GetNewOrientation(orientation, steering.velocity);

		steering.rotation = 0;
		return steering;
	}

	public KinematicSteeringOutput GetSteeringWander(float maxSpeed, float maxRotation) {
		KinematicSteeringOutput steering = new KinematicSteeringOutput();

		Vector3 newVector = new Vector3(Mathf.Sin(orientation), 0, Mathf.Cos(orientation));
		steering.velocity = maxSpeed * newVector;
		Debug.DrawRay(position, newVector);

		float binom = RandomBinomial();
		steering.rotation = binom * maxRotation;

		orientation = steering.rotation;

		return steering;
	}

	public KinematicSteeringOutput GetSteeringArrive(Kinematic target, float maxSpeed, float radius, float timeToTarget) {
		KinematicSteeringOutput steering = new KinematicSteeringOutput();

		steering.velocity = target.position - position;

		//escape if in range
		if (steering.velocity.magnitude < radius) {
			steering.velocity = Vector3.zero;
			Debug.Log("At target");
			return steering;
		}

		steering.velocity /= timeToTarget;

		//cap movement speed
		if (steering.velocity.magnitude > maxSpeed) {
			steering.velocity = steering.velocity.normalized;
			steering.velocity *= maxSpeed;
		}

		//rotate to face (not implemented on walker)
		GetNewOrientation(orientation, steering.velocity);

		steering.rotation = 0;
		return steering;
	}

	public static float RandomBinomial() {
		return Random.Range(0, 1f) - Random.Range(0, 1f);
	}

	public Quaternion Facing() {
		Quaternion toReturn = new Quaternion(velocity.x, 0f, velocity.z, 0f);

		return toReturn;
	}
}

public struct KinematicSteeringOutput {
	public Vector3 velocity;
	public float rotation;
}

public struct SteeringOutput {
	public Vector3 linear;
	public float angular;
}