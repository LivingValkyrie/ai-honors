﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Walker 
/// </summary>
[RequireComponent(typeof (UnitController))]
public class Walker : MonoBehaviour {
	#region Fields

	public MovementBehavior behavior;

	public GameObject targetGo;

	public float arriveRadius;
	public float arriveTime;

	public float maxWanderRotation;

	Kinematic character;
	Kinematic target;
	public float maxSpeed;

	#endregion

	void Start() {
		character = new Kinematic();
	}

	void Update() {
		target.position = targetGo.transform.position;
		character.position = transform.position;

		switch (behavior) {
			case MovementBehavior.Seek:
				transform.Translate(character.GetSteeringSeek(target, maxSpeed).velocity);
				break;
			case MovementBehavior.Flee:
				transform.Translate(character.GetSteeringFlee(target, maxSpeed).velocity);
				break;
			case MovementBehavior.Wander:
				transform.Translate(character.GetSteeringWander(maxSpeed, maxWanderRotation).velocity);

				break;
			case MovementBehavior.Arrive:
				transform.Translate(character.GetSteeringArrive(target, maxSpeed, arriveRadius, arriveTime).velocity);
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}

		transform.forward = Vector3.RotateTowards(transform.position,character.velocity, 1f, 1f);
	}
}

public enum MovementBehavior {
	Seek,
	Flee,
	Wander,
	Arrive
}