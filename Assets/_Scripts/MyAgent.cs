﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: OldAgent
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class MyAgent : MonoBehaviour {
	#region Fields

	public bool drawGizmos = false;
	public AgentState agentState;

	public Vector3 Velocity {
		get { return currVelocity; }
	}
	Vector3 currVelocity;
	public Transform target;
	Vector3 targetPos;
	public float maxSpeed = 10;
	public float maxAcceleration = 10;
	Rigidbody rb;

	[Header("Arrive")]
	public float targetRadius = 2;
	public float slowRadius = 1;
	public float timeToTarget = 0.1f;

	[Header("Path Following")]
	public Transform[] nodes;
	public bool lookAtNodeTarget = false;
	int currNode = 0;

	Transform wanderTarget;
	[Header("Wandering")]
	public float wanderDistance;
	public bool wanderInRegion = false;
	public Transform regionCenter;

	[Header("Flocking")]
	public MyAgent[] flock;
	public float seperationThreshold = 5;
	public float DECAY_COEFFICIENT = 5;

	#endregion

	void Start() {
		rb = GetComponent<Rigidbody>();
		currVelocity = Vector3.zero;

		foreach (Transform node in nodes) {
			node.GetComponent<Renderer>().enabled = false;
		}

		if (GetComponent<KidAgent>()) {
			flock = new MyAgent[GetComponent<KidAgent>().kids.Length - 1];
			int innerCount = 0;
			for (int i = 0; i < GetComponent<KidAgent>().kids.Length; i++) {
				GameObject kid = GetComponent<KidAgent>().kids[i];

				if (kid != gameObject) {
					flock[innerCount] = kid.GetComponent<MyAgent>();
					innerCount++;
				}
			}
		}
	}

	float time = 0;
	float timeToLerp = 2;

	void FixedUpdate() {
		time += Time.fixedDeltaTime;
		if (time > timeToLerp) {
			time = timeToLerp;
		}

		switch (agentState) {
			case AgentState.Seek:
				Seek(ref currVelocity);
				if (GetComponent<KidAgent>()) {
					Seperate(ref currVelocity);
				}
				break;
			case AgentState.Flee:
				Flee(ref currVelocity);
				break;
			case AgentState.Flock:
				Seek(ref currVelocity, true);
				Seperate(ref currVelocity);

				//Center();
				break;
			case AgentState.Follow:
				if (currNode < nodes.Length) {
					target = nodes[currNode];

					//camera stuff
					if (lookAtNodeTarget) {
						if (nodes[currNode].GetComponent<Node>()) {
							//transform.LookAt(nodes[currNode].GetComponent<Node>().target);

							//print( nodes[currNode] );

							transform.LookAt(
								Vector3.Lerp(nodes[Mathf.Max(currNode - 1, 0)].GetComponent<Node>().target.position,
								             nodes[currNode].GetComponent<Node>().target.position,
								             time / timeToLerp)
							);

							//print(Vector3.Lerp(nodes[Mathf.Max(currNode - 1, 0)].GetComponent<Node>().target.position,
							//                   nodes[currNode].GetComponent<Node>().target.position,
							//                   time / timeToLerp));
						}
					}
				} else {
					//camera stuff
					if (lookAtNodeTarget) {
						if (nodes[nodes.Length - 1].GetComponent<Node>()) {
							transform.LookAt(nodes[nodes.Length - 1].GetComponent<Node>().target);
						}
					}
				}

				Arrive(ref currVelocity);

				if (currVelocity == Vector3.zero) {
					currNode++;

					if (lookAtNodeTarget) {
						time = 0;
						//print(time + " time reset, time delta at: " + Time.fixedDeltaTime);
					}

					if (currNode > nodes.Length) {
						currNode = nodes.Length;
					}
				}

				break;
			case AgentState.Wander:
				if (!wanderTarget || !target) {
					print("No wander target, creating one");
					GameObject temp = Instantiate(new GameObject("Wander Target"), transform.position, Quaternion.identity) as GameObject;
					wanderTarget = temp.transform;
				}
				target = wanderTarget;
				Wander();
				break;
			default:
				print(name + " is not in a valid AgentState.");
				break;
		}

		LimitAndMakeFrameIndependent(ref currVelocity);
		Move();
	}

	void Move() {
		Vector3 temp = transform.position;
		temp += currVelocity;
		rb.MovePosition(temp);
	}

	void LimitAndMakeFrameIndependent(ref Vector3 velocity) {
		if (velocity.magnitude > maxSpeed) {
			velocity.Normalize();
			velocity *= maxSpeed;
		}

		velocity *= Time.deltaTime;
	}

	void Seek(ref Vector3 velocity, bool additive = false) {
		Vector3 temp = target.position - transform.position;

		//max acceleration
		velocity.Normalize();
		temp *= maxAcceleration;

		//add or set
		if (additive) {
			velocity += temp;
		} else {
			velocity = temp;
		}
	}

	void Flee(ref Vector3 velocity, bool additive = false) {
		Vector3 temp = transform.position - target.position;

		//max acceleration
		velocity.Normalize();
		temp *= maxAcceleration;

		//add or set
		if (additive) {
			velocity += temp;
		} else {
			velocity = temp;
		}
	}

	void Arrive(ref Vector3 velocity) {
		Vector3 dir = target.position - transform.position;
		float distance = dir.magnitude;
		float targetSpeed = 1;

		if (distance < targetRadius) {
			velocity = Vector3.zero;
			return;
		} else if (distance > slowRadius) {
			targetSpeed = maxSpeed;
		} else {
			targetSpeed = (maxSpeed * distance) / slowRadius;
		}

		dir.Normalize();
		dir *= targetSpeed;

		//todo complete this
		Vector3 targetVel = dir.normalized;
		velocity = targetVel - velocity;
		velocity /= timeToTarget;

		if (velocity.magnitude > maxAcceleration) {
			velocity = velocity.normalized * maxAcceleration;
		}
	}

	void Center() {
		Vector3 total = target.position;
		foreach (MyAgent agent in flock) {
			total += agent.transform.position;
		}

		total /= flock.Length;

		currVelocity += total;
	}

	void Seperate(ref Vector3 velocity, bool additive = true) {
		Vector3 direction;
		Vector3 tempVel = Vector3.zero;

		foreach (MyAgent agent in flock) {
			//todo WHY DID I HAVE TO REVERSE THIS?!?!?!?!?! < book was just wrong.
			if (agent.transform == target || agent == this) {
				//print(name + " is skipping " + agent.name + " with target of " + target);
				continue;
			}

			direction = transform.position - agent.transform.position;
			if (drawGizmos) {
				Debug.DrawLine(transform.position, tempVel, Color.gray);
			}
			if (direction.magnitude < seperationThreshold) {
				float strength = Mathf.Min(DECAY_COEFFICIENT / (direction.magnitude / direction.magnitude), maxAcceleration);

				direction = direction.normalized;
				tempVel += strength * direction;
				if (drawGizmos) {
					Debug.DrawLine(transform.position, tempVel, Color.cyan);
				}
			}
		}

		if (additive) {
			velocity += tempVel;
		} else {
			velocity = tempVel;
		}
	}

	void Wander() {
		Arrive(ref currVelocity);
		if (currVelocity == Vector3.zero) {
			Vector3 newPos = Random.insideUnitSphere * wanderDistance;

			if (wanderInRegion) {
				newPos += regionCenter.position;
			} else {
				newPos += transform.position;
			}

			newPos.y = transform.position.y;

			wanderTarget.position = newPos;
		}
	}

	void OnDrawGizmos() {
		if (drawGizmos) {
			if (target) {
				Gizmos.color = Color.blue;
				Gizmos.DrawWireSphere(target.transform.position, slowRadius);

				Gizmos.color = Color.red;
				Gizmos.DrawWireSphere(target.transform.position, targetRadius);
			}

			if (agentState == AgentState.Wander) {
				Gizmos.color = Color.green;
				if (wanderInRegion && regionCenter) {
					Gizmos.DrawWireSphere(regionCenter.position, wanderDistance);
				} else {
					Gizmos.DrawWireSphere(transform.position, wanderDistance);
				}
			}

			//if (EditorApplication.isPlaying && lookAtNodeTarget) {
			//	Gizmos.color = Color.magenta;
			//	Vector3 pos = Vector3.Lerp(nodes[Mathf.Max(currNode - 1, 0)].GetComponent<Node>().target.position,
			//	                           nodes[currNode].GetComponent<Node>().target.position,
			//	                           time / timeToLerp);
			//	Gizmos.DrawWireSphere(pos,1f);
			//	Handles.Label(pos + Vector3.up, (time / timeToLerp).ToString());
			//	;
			//}
		}
	}

}

public enum AgentState {
	Seek,
	Flee,
	Flock,
	Follow,
	Wander
}