﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: MoveTest
/// </summary>
public class MoveTest : MonoBehaviour {
	#region Fields

	public Vector3 target;
	Rigidbody rb;

	public float speed = 1.0F;
	float startTime;
	float journeyLength;

	#endregion

	void Start() {
		rb = GetComponent<Rigidbody>();
		startTime = Time.time;
		journeyLength = Vector3.Distance(transform.position, target);
	}

	void Update() {
		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength;
		Vector3 temp = Vector3.Lerp(transform.position, target, fracJourney);

		rb.MovePosition(temp);
	}

}